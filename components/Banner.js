import {Jumbotron, Row, Col} from 'react-bootstrap'

//Link component from nextjs for navigation
import Link from 'next/link'

export default function Banner({dataProp}){

	const {title,content,destination,label} = dataProp

	/*Link component is used in NextJS to navigate through pages.
	It uses NextJS routing and can wrap an anchor tag.
	NextJS Link component has href attribute which acts just like our anchor tag href attribute.
	However, the child <a> tag of the Link components is where any styling is applied.
	*/

	return (
		
			<Row className="banner">
				<Col>
					<Jumbotron className= "jumbotronBanner text-center">
						<h1 className="Title">{title}</h1>
						<p>{content}</p>
						<Link href={destination}><a className="Label">{label}</a></Link>
					</Jumbotron>
				</Col>
			</Row>
		


		)
}