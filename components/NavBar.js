import {useContext} from 'react'
import {Navbar, Nav, Container} from 'react-bootstrap'

//import Link component

import Link from 'next/link'

import UserContext from '../userContext'

export default function NavBar() {

	const {user} = useContext(UserContext)

	// console.log(user)

	return (
	<>
		<Navbar bg="dark" expand="lg" variant="dark">
			<Link href="/">
				<a className="navbar-brand">Zeny Tracker</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Link href="/">
						<a className="nav-link" role="button">Home</a>
					</Link>
					
					{
					user.email

					?
						<>
						<Link href="/category">
							<a className="nav-link" role="button">Category</a>
						</Link>
						<Link href="/transactions">
							<a className="nav-link" role="button">Transactions</a>
						</Link>
						<Link href="/charts/monthly-expense">
							<a className="nav-link" role="button">Monthly Expense</a>
						</Link>
						<Link href="/charts/monthly-income">
							<a className="nav-link" role="button">Monthly Income</a>
						</Link>
						<Link href="/charts/incomeVSexpense">
							<a className="nav-link" role="button">Income VS Expense</a>
						</Link>
						<Link href="/logout">
							<a className="nav-link" role="button">Logout</a>
						</Link>
						</>

					: 
					<>

						<Link href="/register">
							<a className="nav-link" role="button">Register</a>
						</Link>

						<Link href="/login">
							<a className="nav-link" role="button">Login</a>
						</Link>

					</>
					}

				</Nav>
			</Navbar.Collapse>
		</Navbar>

		<Navbar fixed="bottom" expand="lg" variant="dark" bg="dark" className="justify-content-center">
  			
    			<Navbar.Text>
      				Full-stack Web Developer: <a href="#">Junnmer Fajardo</a>
    			</Navbar.Text>
  			
		</Navbar>

	</>
		)
}