import Head from 'next/head' //allows us to add a head tag into this page's HTML.
import {Button,Form,Row,Col} from 'react-bootstrap'
import Link from 'next/link'
import {useState,useEffect,useContext,Fragment} from 'react'
import UserContext from '../../userContext'
import Router from 'next/router'
import Swal from 'sweetalert2'


export default function newCategory(){

	/* track user input in realtime */
	const[categoryName,setCategoryName] = useState("")
	const[categoryType,setCategoryType] = useState("")
	//Submit button
	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(categoryName !== "" && categoryType !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[categoryName,categoryType])

	// get the user
	const {user} = useContext(UserContext)
	console.log(user)

	function newCategory(e){
		e.preventDefault()

		//Get Token from local storage
		// let token = localStorage.getItem("token")

		// Create a fetch request to add a new category
		fetch('https://damp-mountain-15439.herokuapp.com/api/users/addCategory', {
		method: 'POST',
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			category: categoryName,
			categoryType: categoryType
		})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true){

			Swal.fire({
				icon:"success",
				title:"Successfully Added a Category.",
				text:"Thank you!"

			})

			Router.push("/category")

			} else {
				Swal.fire({
					icon: "error",
					title: "Unsuccessful.",
					text: "Creation Failed."

				})

			}
		
	
		})


	}



	return(

		<Row className="categoryPage">
			<Col>

				<Head><title>Zeny Tracker</title></Head>
				<Form className="text-white" onSubmit={e => newCategory(e)}>

				  <Form.Group controlId="categoryName">
				    <Form.Label>Category Name</Form.Label>
				    <Form.Control type="text" placeholder="Enter category name" value={categoryName} onChange={e =>setCategoryName(e.target.value)} required/>
				  </Form.Group>
				  <Form.Group controlId="categoryType">
				    	<Form.Label>Category Type</Form.Label>
						    <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} required>
						    	<option> </option>
						      	<option>Income</option>
						      	<option>Expense</option>
						    </Form.Control>
				  </Form.Group>
				  
				  {
				  	isActive
				  	?
					<Button variant="dark" type="submit">Create</Button>
					:
					<Button variant="dark" disabled>Create</Button>
				  }
				  
				</Form>
				
	
			</Col>
		</Row>
	

		)

}