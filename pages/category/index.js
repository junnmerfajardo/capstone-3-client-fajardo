import Head from 'next/head' //allows us to add a head tag into this page's HTML.
import {Button,Table,Row,Col} from 'react-bootstrap'
import Link from 'next/link'
import {useState,useEffect,useContext,Fragment} from 'react'
import UserContext from '../../userContext'



export default function addCategory(){

	// get the user
	const {user} = useContext(UserContext)
	console.log(user)

	/*getting TOKEN using state*/
	// const[token,setToken] = useState("")

	// useEffect(()=>{

	// setToken(

	// 	localStorage.getItem("token")

	// 	)

	// },[])
	// console.log(token)



	// let userId = localStorage.getItem("userId")
	// console.log(userId)
	//Get categories and type
	const [categories,setCategories] = useState([])

	useEffect(()=>{

		fetch('https://damp-mountain-15439.herokuapp.com/api/users/details', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCategories(data.categories)

		})

	},[])

	console.log(categories)

	const categoriesRows = categories.map(categories =>{

		return(
			<tr key={categories._id}>
				<td>{categories.categoryName}</td>
				<td>{categories.categoryType}</td>
			</tr>
			)

	})

	return(
		<>
			<Head><title>Zeny Tracker</title></Head>
			<h1 className="mb-3 text-white">Categories</h1>
			<Link href="/category/new">Add A Category</Link>
			<Table className="text-white categoryPage" striped bordered size="lg">
			  <thead>
			    <tr>
			      <th>Category</th>
			      <th>Type</th>
			    </tr>
			  </thead>
			  <tbody>
			    {categoriesRows}
			  </tbody>
			</Table>
		</>
	

		)
}