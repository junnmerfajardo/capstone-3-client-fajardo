import {useState,useEffect,useContext} from 'react'
import {Form,Button,Card,Row, Col} from 'react-bootstrap'
import Swal from 'sweetalert2'
import Head from 'next/head' //allows us to add a head tag into this page's HTML.



//to redirect the user, use the Router component from nextjs
import Router from 'next/router'

import UserContext from '../../userContext'

//import Google login component
import {GoogleLogin} from 'react-google-login'

export default function Login() {

	const {user, setUser} =useContext(UserContext)
	console.log(user)

	//create states for user input
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	//create a state for conditionally rendering our button

	const [isActive,setIsactive] = useState(true)

	useEffect(()=>{

			if(email !== "" && password !== ""){

				setIsactive(true)

			} else {

				setIsactive(false)

			}

	},[email, password])

	function authenticate(e){
		e.preventDefault()

		fetch('https://damp-mountain-15439.herokuapp.com/api/users/login',{

			method: "POST",
			headers: {

				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				
				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.accessToken){

				localStorage.setItem("token", data.accessToken)
				fetch('https://damp-mountain-15439.herokuapp.com/api/users/details', {

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}

				})
				.then(res => res.json())
				.then(data => {

					console.log(data)
					localStorage.setItem('email',data.email)
					localStorage.setItem('userId',data._id)
					//after getting the userdatails from the api server we will set the global user state.
					setUser({

						email: data.email,
						id: data._id

					})

				})

				Swal.fire({
				icon: "success",
				title: "Successfully Logged In,",
				text: "Thank you for logging in."

				})

				/* Router component's push method will redirect the user to the endpoint given as an argument to the method.
				*/
				Router.push('/category')

			} else {

				Swal.fire({

				icon: "error",
				title: " Unsuccessful Login.",
				text: "User authentication failed."

				})

			}

		})

		//set the input states into their initial value
		setEmail("")
		setPassword("")

		
	}

	function authenticateGoogleToken(response){
		//google's response with our tokenId to be used to authenticate our google login user.
		console.log(response)

		/*For Email Sending:

			Pass the accessToken from google to allow us the use of Google API to send an email to the google login user who logs on for the first time.

		*/

		fetch('https://damp-mountain-15439.herokuapp.com/api/users/verify-google-id-token', {

			method: 'POST',
			headers: {

				'Content-Type' : 'application/json'

			},
			body: JSON.stringify({

				tokenId: response.tokenId,
				accessToken: response.accessToken

			})
		})
		.then(res => res.json())
		.then(data => {
			
			//we will show alerts to show if the user logged in properly or if there are errors.
			if(typeof data.accessToken !== 'undefined'){

				//set the accessToken into our localStorage as token:
				localStorage.setItem('token', data.accessToken)

				//run a fetch request to get our user's details and update our global user state and save our user details into the localStorage:
				fetch('https://damp-mountain-15439.herokuapp.com/api/users/details',{

					headers: {

						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('email', data.email)
					localStorage.setItem('isAdmin', data.isAdmin)

					//after getting the user's details, update the global user state:
					setUser({
						email: data.email,
						isAdmin: data.isAdmin
					})

					//Fire a sweetalert to inform the user of successful login:
					Swal.fire({

						icon: 'success',
						title: 'Successful Login'
					})

					Router.push('/category')
				})
			} else {

				//if data.accessToken is undefined, therefore, data contains a property called error instead.
				if(data.error === "google-auth-error"){

					Swal.fire({
						icon: "error",
						tittle: 'Google Authentication Failed'

					})

				} else if(data.error === "login-type-error"){

					Swal.fire({
						icon: "error",
						tittle: "Login Failed",
						text: "You may have registered through a different procedure."
					})
				}
			}
		})
	}

	// function failed(response){
	// 	console.log(response)
	// }

	return(
		<>
		<Head><title>Zeny Tracker</title></Head>
		<Row className="loginregPage">
			<Col>
					<Form className="loginregForm mt-3" onSubmit={e => authenticate(e)}>
						<Form.Group controlId="userEmail">
							<Form.Label>Email:</Form.Label>
							<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="userPassword">
							<Form.Label>Password:</Form.Label>
							<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
						</Form.Group>

						<GoogleLogin 

							clientId="376687092718-i0mhpu2cn819nihid5s37587gr98r4l4.apps.googleusercontent.com"
							buttonText="Login Using Google"
							onSuccess={authenticateGoogleToken}
							onFailure={authenticateGoogleToken}
							cookiePolicy={'single_host_origin'}
							className="w-30 text-center mb-3 d-flex justify-content-center"

						/>

						{
							isActive
							?
							<Button className="mb-3" variant="light" type="submit">Submit</Button>
							:
							<Button className="mb-3" variant="light" disabled>Submit</Button>
						}

					</Form>
			</Col>
		</Row>
		</>
		)
}