import Head from 'next/head' //allows us to add a head tag into this page's HTML.

import {Bar} from 'react-chartjs-2'

import moment from 'moment'

import {useState,useEffect} from 'react'

import {Row,Col} from 'react-bootstrap'

// import UserContext from '../../userContext'


export default function monthlyexpense(){

	// get the user
	// const {user} = useContext(UserContext)
	// console.log(user)

	//states for processed data to be plotted
	const [months,setMonths] = useState([]);
	const [monthlyExpenses,setMonthlyExpenses] = useState([]);
	const [allTransactions,setAllTransactions] = useState([]);

	useEffect(()=>{

		fetch('https://damp-mountain-15439.herokuapp.com/api/transactions', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllTransactions(data.data)

		})

	},[])
	//
	//months
	useEffect(() => {

		if(allTransactions.length > 0){

			let tempMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			allTransactions.forEach(element => {

				if(!tempMonths.find(month => month === moment(element.createdAt).format('MMMM'))){

					tempMonths.push(moment(element.createdAt).format('MMMM'))

				}

			})

			const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)

				}

			})

			setMonths(tempMonths)

		}

	},[allTransactions])

	//monthly expense
	useEffect(() => {

		setMonthlyExpenses(months.map(month => {

			let expense = 0

			allTransactions.forEach(element => {

				if(moment(element.createdAt).format("MMMM") === month && element.transactionType === "Expense"){

					expense += parseInt(element.amount)

				}

			})
			return expense

		}))

	},[months])


	//
	//
	const expenses = {
		labels: months,
		datasets: [{
			label: 'Monthly Expense in PHP',
			backgroundColor: 'rgba(255, 82, 82, 0.55)',
			// borderColor: 'rgba(0, 0, 0, 0.94)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 82, 82, 0.94)',
			hoverBorderColor: 'rgba(0, 0, 0, 0.94)',
			data: monthlyExpenses//determinand for bars
		}]
	}

	const options = {
		legend: {
			labels:{
            	fontColor: "rgba(255, 82, 82, 0.94)",
            	fontSize:20

			}
        },
		scales: {
			yAxes:[{
					ticks: {
						beginAtZero: true,
						fontColor: 'white'
						// max: 5000
					},
					gridLines:{
          				color: "rgba(8, 144, 181, 0.33)",
          				lineWidth:2,
          				zeroLineColor :"rgba(8, 144, 181, 0.33)",
          				zeroLineWidth : 2
        			},

				}],
			xAxes:[{
					ticks: {
						fontColor: 'white'
						// max: 5000
					},
					gridLines:{
          			color: "rgba(8, 144, 181, 0.33)",
          			lineWidth:2
        			}
				}],
		}
	}


	return(
		
	<>
	<Row className="chart">
	<Head><title>Zeny Tracker</title></Head>
		<Col className="text-center text-white">
			<Bar data={expenses} options={options}/>
		</Col>
	</Row>
	
	</>
		)



}