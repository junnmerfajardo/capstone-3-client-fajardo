import Head from 'next/head' //allows us to add a head tag into this page's HTML.

import {Pie} from 'react-chartjs-2'

import moment from 'moment'

import {useState,useEffect} from 'react'

import {Row,Col} from 'react-bootstrap'

// import UserContext from '../../userContext'


export default function incomevsexpense(){

	// get the user
	// const {user} = useContext(UserContext)
	// console.log(user)

	//states for processed data to be plotted
	const [totalIncome,setTotalIncome] = useState([]);
	const [totalExpenses,setTotalExpenses] = useState([]);
	const [allTransactions,setAllTransactions] = useState([]);

	useEffect(()=>{

		fetch('https://damp-mountain-15439.herokuapp.com/api/transactions', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllTransactions(data.data)

		})

	},[])
	//
	//income/xpense
	useEffect(() => {

		setTotalIncome(allTransactions.map(result => {

			let income = 0

			allTransactions.forEach(element => {

				if(element.transactionType === "Income"){

					income += parseInt(element.amount)

				}

			})
			return income

		}))

	},[allTransactions])

	useEffect(() => {

		setTotalExpenses(allTransactions.map(result => {

			let expense = 0

			allTransactions.forEach(element => {

				if(element.transactionType === "Expense"){

					expense += parseInt(element.amount)

				}

			})
			return expense

		}))

	},[allTransactions])

	//
	//
	const totalIncomeRes = totalIncome[0]
	const totalExpensesRes = totalExpenses[0]

	const data = {

		datasets:[{
			data: [totalIncomeRes,totalExpensesRes],
			backgroundColor: ["rgba(77, 166, 63, 0.3)","rgba(255, 82, 82, 0.55)"],
			borderColor: "rgba(8, 144, 181, 0.33)"
		}],
		labels: ["Total Income","Total Expenses"]

	}
	//
	const options = {
		legend: {
			labels:{
            	fontColor: "white",
            	fontSize: 20

			}
        }

	}
	


	return(
		
	<>
	<Row className="chart">
	<Head><title>Zeny Tracker</title></Head>
		<Col className="text-center text-white">
			<Pie data={data} options={options}/>
		</Col>
	</Row>
	
	</>
		)



}