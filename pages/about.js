import Head from 'next/head' //allows us to add a head tag into this page's HTML.

import {Row,Col,Jumbotron,Card} from 'react-bootstrap'
import Link from 'next/link'


export default function aboutPage(){

	return(
		
	<>
	<Row className="banner">
		<Col>
		<Head><title>Zeny Tracker</title></Head>
			<Jumbotron className= "jumbotronBanner text-center">
				<h1 className="Title">A simple budget tracker</h1>
				<p><em>Hi! my name is Jm, a full-stack web developer.</em></p>
				<p>You can start using this app by <em>registering.</em></p>
				<p>You can also login using your google account, don't worry your password will not be stored in this web app.</p>
				<p>After login, you can start adding a <em>Category</em> for <em>Expenses and Income,</em> then you can use that to add <em>Transactions.</em></p>
				<p>You can also monitor your <em>Monthly Expense</em> and <em>Monthly Income.</em></p>
				<p>Lastly, you can view an overall comparison of your income v.s expenses</p>
				<h6>
					If you have any questions or suggestions, plese click the link below:
				</h6>
				<h5>
					<div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="en_US" data-type="horizontal" data-theme="light" data-vanity="junnmer-fajardo-300b2219b"><a class="LI-simple-link" href='https://ph.linkedin.com/in/junnmer-fajardo-300b2219b?trk=profile-badge' target="blank">Junnmer Fajardo</a></div>
				</h5>
			</Jumbotron>
		</Col>
	</Row>
	
	</>

		)



}