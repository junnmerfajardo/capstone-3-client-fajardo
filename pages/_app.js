import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'

//hooks from react
import {useState,useEffect} from 'react'

//Bootstrap Components
import {Container} from 'react-bootstrap'

//import Navbar
import NavBar from '../components/NavBar'

//import our UserProvider
import {UserProvider} from '../userContext'

/*
NextJS uses the App component to initialize our pages. However, routing is already done by NextJS for us, Here in the App.js our component is receied as a prop. The active page is the Component prop received in this App component.

*/

function MyApp({ Component, pageProps }) {

	const [user,setUser] = useState({

		email: null,
		id: null
	})

	/*
	NextJS is pre-rendered.
	Your pages are built initially in the server then passed into browser.
	Localstorage does not exist, until the page/component is rendered.
	We are going to use useEffect to get our email and isAdmin details from our localStorage instead so as to assure that we access that localStorage only after the page/component has initially rendered.
	*/

useEffect(()=>{

	setUser({

		email: localStorage.getItem('email'),
		id: localStorage.getItem('userId')
	})

},[])

const unsetUser = () =>{

	localStorage.clear()

	// set the values of our state back to its initials values
	setUser({

		email: null,
		id: null
	
	})

}

/*
Pass the setter function for user state and the unsetUser function to all of our components.
*/


  return (

  	<>
  		 <UserProvider value={{user,setUser,unsetUser}}>
  			<NavBar />
	  			<Container>
	  				<Component {...pageProps} />
	  			</Container>
  		</UserProvider>
  	</>


  	) 
}

export default MyApp
