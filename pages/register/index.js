import {useEffect,useState,useContext} from 'react'
import {Form,Button,Row,Col} from 'react-bootstrap'
import Head from 'next/head' //allows us to add a head tag into this page's HTML.


//import router
import Router from 'next/router'

//import swal
import Swal from 'sweetalert2'

//import Google login component
import {GoogleLogin} from 'react-google-login'

export default function Register() {

	/*
	track user input in realtime
	*/
	const[firstName,setFirstName] = useState("")
	const[lastName,setLastName] = useState("")
	const[email,setEmail] = useState("")
	const[password1,setPassword1] = useState("")
	const[password2,setPassword2] = useState("")

	/*state for conditionally rendering submit button*/
	const [isActive,setIsActive] = useState(true)

	//will run on every change to our user's input
	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[firstName,lastName,email,password1,password2])



	function registerUser(e){

		e.preventDefault()


		fetch('https://damp-mountain-15439.herokuapp.com/api/users/email-exists', {
		method: 'POST',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify({
			email: email
			})
		})

		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === false) {

				fetch('https://damp-mountain-15439.herokuapp.com/api/users', {
					method: 'POST',
					headers: {'Content-Type': 'application/json'},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {

						console.log(data);

						if(data === true){

							Swal.fire({

								icon:"success",
								title:"Successfully Registered.",
								text:"Thank you for registering."

								})	

							//redirect to login page
							Router.push("/login")

						} else {
							//error in creating registration
							Swal.fire({

								icon: "error",
								title: "Registration Failed.",
								text: "User Registration failed."

							})
						}
					})
				} else {
						//email already exist
					Swal.fire({

								icon: "error",
								title: "Email Already Exist.",
								text: "User Registration failed."

							})


				}
			})

		//clear input fields
		setFirstName("")
		setLastName("")
		setEmail("")
		setPassword1("")
		setPassword2("")
		
	}

	function authenticateGoogleToken(response){
		//google's response with our tokenId to be used to authenticate our google login user.
		console.log(response)

		/*For Email Sending:

			Pass the accessToken from google to allow us the use of Google API to send an email to the google login user who logs on for the first time.

		*/

		fetch('https://damp-mountain-15439.herokuapp.com/api/users/verify-google-id-token', {

			method: 'POST',
			headers: {

				'Content-Type' : 'application/json'

			},
			body: JSON.stringify({

				tokenId: response.tokenId,
				accessToken: response.accessToken

			})
		})
		.then(res => res.json())
		.then(data => {
			
			//we will show alerts to show if the user logged in properly or if there are errors.
			if(typeof data.accessToken !== 'undefined'){

				//set the accessToken into our localStorage as token:
				localStorage.setItem('token', data.accessToken)

				//run a fetch request to get our user's details and update our global user state and save our user details into the localStorage:
				fetch('https://damp-mountain-15439.herokuapp.com/api/users/details',{

					headers: {

						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('email', data.email)
					localStorage.setItem('isAdmin', data.isAdmin)

					//after getting the user's details, update the global user state:
					setUser({
						email: data.email,
						inAdmin: data.isAdmin
					})

					//Fire a sweetalert to inform the user of successful login:
					Swal.fire({

						icon: 'success',
						title: 'Successful Login'
					})

					Router.push('/category')
				})
			} else {

				//if data.accessToken is undefined, therefore, data contains a property called error instead.
				if(data.error === "google-auth-error"){

					Swal.fire({
						icon: "error",
						tittle: 'Google Authentication Failed'

					})

				} else if(data.error === "login-type-error"){

					Swal.fire({
						icon: "error",
						tittle: "Login Failed",
						text: "You may have registered through a different procedure."
					})
				}
			}
		})
	}

	return (

		<Row className="loginregPage">
			<Col>
			<Head><title>Zeny Tracker</title></Head>

				<Form className="loginregForm mt-5" onSubmit={e => registerUser(e)}>
					<Form.Group controlId="userFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="userLastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="userEmail">
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
					</Form.Group>

					<GoogleLogin 

									clientId="376687092718-i0mhpu2cn819nihid5s37587gr98r4l4.apps.googleusercontent.com"
									buttonText="Login Using Google"
									onSuccess={authenticateGoogleToken}
									onFailure={authenticateGoogleToken}
									cookiePolicy={'single_host_origin'}
									className="w-30 text-center my-3 d-flex justify-content-center"

								/>
					{
						isActive
						?
						<Button className="mb-3" variant="light" type="submit">Register</Button>
						:
						<Button className="mb-3" variant="light" disabled>Register</Button>
					}


				</Form>
			</Col>
		</Row>

		)
}