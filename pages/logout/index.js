import {useContext,useEffect} from 'react'

import UserContext from '../../userContext'

//redirect, user Router component from NextJS
import Router from 'next/router'

export default function Logout(){

	const {unsetUser} = useContext(UserContext)

	useEffect(()=>{

		unsetUser()

		//redirect user to the login page

		Router.push('/login')

	},[])

	return null

}