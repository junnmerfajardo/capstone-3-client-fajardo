import Head from 'next/head' //allows us to add a head tag into this page's HTML.
import {Button,Row,Col,Form,Card,Jumbotron,Table} from 'react-bootstrap'
import Link from 'next/link'
import {useState,useEffect,useContext,Fragment} from 'react'
import UserContext from '../../userContext'




export default function addTransactions(){

	// get the user
	const {user} = useContext(UserContext)
	// console.log(user)

	const [allTransactions,setAllTransactions] = useState([]);
	const [search,setSearch] = useState("");
	const [filteredTrans, setFilteredTrans] = useState([]);
	const [selector,setSelector] = useState("");
	const [records,setRecords] = ([]);

	useEffect(()=>{

		fetch('https://damp-mountain-15439.herokuapp.com/api/transactions', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllTransactions(data.data)

		})

	},[])
	// console.log(allTransactions)
	//BALANCE
	const positiveData = allTransactions.map(pos => {
	if(pos.transactionType == "Income"){
		return parseInt(pos.amount)
		}
	})

	const negativeData = allTransactions.map(neg => {
	if(neg.transactionType == "Expense"){
		return -Math.abs(parseInt(neg.amount))
		}
	})

	const posNeg = positiveData.concat(negativeData)
	const filteredPosNeg = posNeg.filter(data => {
		return data != undefined
	})

	const balance = filteredPosNeg.reduce((accumulator,currentValue) => {
		return accumulator + currentValue
	},0)
	console.log("Income",positiveData)
	console.log("Expense",negativeData)
	console.log(posNeg)

	// console.log(allTransactions)
	//Expense Income IF
	//
// 	useEffect(()=>{

// 		fetch('http://localhost:8000/api/transactions', {
// 			headers: {
// 				'Authorization': `Bearer ${localStorage.getItem("token")}`
// 			}
// 		})
// 		.then(res => res.json())
// 		.then(data => {

// 			const allArr =[];
// 			const recordIncome =[];
// 			const recordExpense = [];


// 		if(selector === "Income"){
// 			data.filter(result =>{
				
// 				if(result.transactionType === "Income"){
// 					recordIncome.push(result)
// 				}
// 			})
// 			setRecords(recordIncome)
// 		}else if (selector === "Expense"){
// 			data.filter(result =>{
				
// 				if(result.transactionType === "Expense"){
// 					recordExpense.push(result)
// 				}
// 			})
// 			setRecords(recordExpense)
// 		}else if (selector === "All"){
// 			data.filter(result =>{
				
// 			allArr.push(result)
				
// 			})
// 			setRecords(allArr)	
// 		}

// 	})
		
// },[selector])

// 	const allRecords = records.map(record => {
// 	return 
// 		<Card key= {record._id} className="border-white text-white my-3 transactionPage">
// 	 		<Card.Body>
// 	 		    <Card.Title>{record.description}</Card.Title>
// 	 		    <Card.Text>{record.transactionType}</Card.Text>
// 	 		    <Card.Text>Amount: {record.amount}</Card.Text>
// 	 		    <Card.Text>Added On: {record.createdAt}</Card.Text>
// 	 		</Card.Body>
// 		</Card>
// 	})
//
//


	// const allTransactionsCards = allTransactions.map(transactions => {
	// 	console.log(allTransactionsCards)
	// 	return (
	// 		<Card key= {transactions._id} className="border-white text-white my-3 transactionPage">
	// 		  <Card.Body>
	// 		    <Card.Title>{transactions.description}</Card.Title>
	// 		    <Card.Text>{transactions.transactionType}</Card.Text>
	// 		    <Card.Text>Amount: {transactions.amount}</Card.Text>
	// 		    <Card.Text>Added On: {transactions.createdAt}</Card.Text>
	// 		  </Card.Body>
	// 		</Card>
	// 		)
			
	// })

	//
	//state for searching transaction via description
	console.log(allTransactions)
    useEffect(()=>{
    	setFilteredTrans(
    		allTransactions.filter(transactions => {
    			return (
    				transactions.description.toLowerCase().includes(search.toLowerCase()) || transactions.transactionType.toLowerCase().includes(search.toLowerCase())
    				)
    		})
    		)
    },[search])


    const finalFilter = filteredTrans.map(result => {

	return(
			<Card key= {result._id} className="border-white text-white my-3 transactionPage">
	 		<Card.Body>
	 		    <Card.Title>{result.description}</Card.Title>
	 		    <Card.Text>{result.transactionType}</Card.Text>
	 		    <Card.Text>Amount: {result.amount}</Card.Text>
	 		    <Card.Text>Added On: {result.createdAt}</Card.Text>
	 		  </Card.Body>
			</Card>
		)
	})

	return(
		<div className="test">
			<Head><title>Zeny Tracker</title></Head>
			<h1 className="text-white">Transaction Records</h1>
			<Link href="/transactions/new">Add A Transaction</Link>
			<Table className="text-white">
				<thead>
			    <tr>
			      <th>Balance</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			    	<td>{balance}</td>
			    </tr>
			  </tbody>
			</Table>
			<Form>
                <Form.Group controlId="searchTransaction">
                    <Form.Control type="text" placeholder="Search by Description" value={search} onChange={e => setSearch(e.target.value)} required/>
                </Form.Group>
                	
                <Form.Group controlId="transaction-filter">
					<Form.Control as="select" value={search} onChange={e => setSearch(e.target.value)}>
						<option> </option>
						<option>Income</option>
						<option>Expense</option>
					</Form.Control>
				</Form.Group>
				<div>
					{
						// search !== "" ? 
						finalFilter
						// : allRecords

					}
				</div>

            </Form>
		</div>


		)
}

/*






*/