import Head from 'next/head' //allows us to add a head tag into this page's HTML.
import {Button,Form,Row,Col} from 'react-bootstrap'
import Link from 'next/link'
import {useState,useEffect,useContext,Fragment} from 'react'
import UserContext from '../../userContext'
import Router from 'next/router'
import Swal from 'sweetalert2'


export default function newTransactions(){

// get the user
	const {user} = useContext(UserContext)
	console.log(user)

	/* track user input in realtime */
	const [categoryType,setCategoryType] = useState("");
	const [categoryName,setCategoryName] = useState("");
	const [description,setDescription] =useState("");
	const [amount,setAmount] =useState(0);

	//Submit button
	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(categoryName !== "" && categoryType !== "" && description !== "" && amount !== 0){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[categoryName,categoryType, description, amount])



	//Get categories and type
	const [categories,setCategories] = useState([])


	useEffect(()=>{

		fetch('https://damp-mountain-15439.herokuapp.com/api/users/details', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCategories(data.categories)

		})

	},[])

	console.log(categories)
	const categoriesOptions = categories.map(categories =>{

		return(
			<option key={categories._id}>
			{categories.categoryName}
			</option>
			)

	})


	function addTransaction(e){
		e.preventDefault()

		//Get Token from local storage
		// let token = localStorage.getItem("token")

		// Create a fetch request to add a new transaction
		fetch('https://damp-mountain-15439.herokuapp.com/api/transactions', {
		method: 'POST',
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			transactionType: categoryType,
			transactionName: categoryName,
			description: description,
			amount: amount,
			userId:`${localStorage.getItem("userId")}`

		})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			console.log(data.data)

			if(data){

			Swal.fire({
				icon:"success",
				title:"Transaction Added.",
				text:"Thank you!"

			})

			Router.push("/transactions")

			} else {
				Swal.fire({
					icon: "error",
					title: "Unsuccessful.",
					text: "Creation Failed."

				})

			}
		
	
		})


	}



	return(

		// <h1>test</h1>
	<>
		<Head><title>Zeny Tracker</title></Head>
		<Row className="loginregPage">
			<Col>
				<Form onSubmit={e=> addTransaction(e)} className="loginregForm mt-3"> 

					<Form.Group controlId="selectionLabel">
						<Form.Label>Transaction Type</Form.Label>
						<Form.Control as="select" value={categoryType} onChange={e=> setCategoryType(e.target.value)}>
							<option value="" disabled></option>  
							<option>Income</option>		      
							<option>Expense</option>
						</Form.Control>
					</Form.Group>
					<Form.Group controlId="selectionLabel">
						<Form.Label>Transaction Name</Form.Label>
						<Form.Control as="select" value={categoryName} onChange={e=> setCategoryName(e.target.value)}>	
							<option value="" disabled></option>     
									{categoriesOptions}					
						</Form.Control>
					</Form.Group>

					<Form.Group controlId="transactionDesc">
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" placeholder="Description" value={description} onChange={e=> setDescription(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="transactionAmount">
						<Form.Label>Amount</Form.Label>
						<Form.Control type="number" placeholder="Amount" value={amount} onChange={e=> setAmount(e.target.value)} required/>
					</Form.Group>

					{
						isActive
						?
							<Button variant="dark" type="submit">Add</Button>
						:
							<Button variant="dark" disabled>Add</Button>
					}
				</Form>	
			</Col>
		</Row>
	</>
		)

}




