import Banner from '../components/Banner'
import Head from 'next/head' //allows us to add a head tag into this page's HTML.


export default function Home() {

const data = {
	title: "Zeny Tracker",
	content: "A Simple Expense Tracker",
	destination: '/about',
	label: 'Get Started!'
}	

/*

*/

  return (
  		<>
  			<Head><title>Zeny Tracker</title></Head>
	      	<Banner dataProp={data}/>
    	</>


    )
    
}